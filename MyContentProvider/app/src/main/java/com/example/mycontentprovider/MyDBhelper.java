package com.example.mycontentprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class MyDBhelper extends SQLiteOpenHelper {

    public MyDBhelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("create table student(id integer primary key autoincrement " +
                ",name varchar(20) ," +
                "age integer)");  //execSQL  最简单粗暴的语句   可以执行标准的SQL语句
        //写到这里发现MyDBhelper还是灰色->未被使用->databaseactivity中操作
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
