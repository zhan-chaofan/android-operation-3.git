package com.example.mycontentprovider;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

public class MyDAO {
    private SQLiteDatabase database;
    private SQLiteOpenHelper myopenhelper;
    private Context context;

    private Uri uri=Uri.parse("content://xr.provider1");

    public MyDAO(Context context){
        this.context=context;
        myopenhelper=new MyDBhelper(context,"xrDB",null,1);
        database=myopenhelper.getReadableDatabase();

        database.execSQL("drop table if exists student");
        database.execSQL("create table student(id integer primary key autoincrement,"+" name varchar, age integer)");
    }

    public Uri addvalue(Uri uri, ContentValues values){
        long rowID=database.insert("student",null,values);

        if(rowID == -1){
            Log.d("DAO","数据插入失败");
            return  null;
        }
        else {
            Uri insertUri= ContentUris.withAppendedId(uri,rowID);
            Log.d("xr","ContentUris:"+insertUri.toString());
            context.getContentResolver().notifyChange(insertUri,null);
            return insertUri;
        }
    }


}
